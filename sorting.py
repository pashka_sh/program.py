from program import print_list

numbers = [123, 321,3 , 32 , 5 ,6]
# names = ["Alish", "Pasha" , "Baha", "Anton"]

# names.sort()
# d.sort(reverse=True)
#
# print_list(d)
# print ("----")
# print_list(names)

def my_sort(array):

    for i in range(len(array)):
        for j in range(i+1,len(array)):

            if array[j] > array[i]:
                array[i],array[j] = array[j],array[i]

    return array

print_list(my_sort(numbers))
